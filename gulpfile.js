var gulp = require('gulp');

var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    minify = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

gulp.task('lint', function() {
    return gulp.src('assets/javascript/frontend/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('sass', function() {
    return gulp.src(['bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap.scss', 'assets/stylesheet/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest('assets/stylesheet'))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('assets/stylesheet'))
        .pipe(rename('all.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('assets/stylesheet'));
});

gulp.task('angular', function() {
    return gulp.src([
        'bower_components/angular*/angular*.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-socket-io/socket.js',
        'assets/javascript/frontend/angular*.js',
        '!bower_components/angular*/*.min.js'
    ])
        .pipe(concat('angular-all.js'))
        .pipe(gulp.dest('assets/javascript'));
});

gulp.task('frontend', function() {
    return gulp.src(['bower_components/jquery/dist/jquery.js', 'assets/javascript/frontend/*.js', '!assets/javascript/frontend/angular*.js'])
        .pipe(concat('frontend.js'))
        .pipe(gulp.dest('assets/javascript'))
        .pipe(rename('frontend.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/javascript'));
});

gulp.task('watch', function() {
    gulp.watch(['assets/javascript/frontend/*.js', '!assets/javascript/frontend/angular-conf.js'], ['lint', 'frontend', 'angular']);
    gulp.watch('assets/javascript/frontend/angular-conf.js', ['angular']);
    gulp.watch('assets/stylesheet/scss/*.scss', ['sass']);
});

gulp.task('default', ['lint', 'sass', 'angular', 'frontend', 'watch']);