var L = require('lazy.js');
// You must be aware that date in real app would be Date object
var items = [
	{
		type : 'item',
		name : 'Kondom Sutra',
		category : 'Obat-obatan dan kesehatan',
		subCategory : 'Kondom',
		supplier : 'Mat Solar',
		barcode : ['192294283', '192039483'],
		stock : 40,
		minStock : 10,
		rating : 3,
		amountType : 'piece',
		bought : [{ price : 10000, qty : 20 }, { price : 11000, qty : 20 }],
		markUpFee : 0.20,
		handlingFee : 0.10,
		GST : false,
		picture : { src : '', category : 'Kondom Sutra' }
	},
	{
		type : 'item',
		name : 'Kondom Fiesta',
		category : 'Obat-obatan dan kesehatan',
		subCategory : 'Kondom',
		supplier : 'Mat Solar',
		barcode : ['192392949'],
		stock : 10,
		minStock : 5,
		rating : 3,
		amountType : 'piece',
		bought : [{ price : 14000, qty : 10 }],
		markUpFee : 0.20,
		handlingFee : 0.10,
		GST : false,
		picture : { src : '', category : 'Kondom Fiesta' }
	},
	{
		type : 'item',
		name : 'Kalpanax',
		category : 'Obat-obatan dan kesehatan',
		subCategory : 'Salep kulit',
		supplier : 'Mat Solar',
		barcode : ['940394839'],
		stock : 100,
		minStock : 10,
		rating : 4,
		amountType : 'piece',
		bought : [{ price : 5000, qty : 10 }, { price : 4500, qty : 50 }, { price : 4000, qty : 40 }],
		markUpFee : 0.20,
		handlingFee : 0.10,
		GST : false,
		picture : { src : '', category : 'Salep Kalpanax' }
	},
	{
		type : 'item',
		name : 'Chiki',
		category : 'Makanan',
		subCategory : 'Snack',
		supplier : 'Dul Joni',
		barcode : ['382938493', '382948392', '222839203'],
		stock : 190,
		minStock : 80,
		rating : 5,
		amountType : 'piece',
		bought : [{ price : 1000, qty : 100 }, { price : 1100, qty : 100 }],
		markUpFee : 0.20,
		handlingFee : 0.10,
		GST : false,
		picture : { src : '', category : 'Chiki Snack' }
	}
];

transactionIn = [
	{
		supplierName : 'John', 
		date : Date.now(),
		items : [
			{ name : 'sutra', qty :10, priceEach : 12000.00, totalPrice : 12000.00*10 },  
			{ name : 'extra joss', qty : 30, priceEach : 1000.00, totalPrice : 1000.00*30 }
		]
	}, {
		supplierName : 'John', 
		date : Date.now() + (1000 * 3600 * 24 * 2),
		items : [{ name : 'sutra', qty : 5, priceEach : 12000.00, totalPrice : 12000.00*5 }]
	}, {
		supplierName : 'Jane', 
		date : Date.now() + (1000 * 3600 * 24 * 2),
		items : [{ name : 'sutra', qty : 1, priceEach : 12000.00, totalPrice : 12000.00*1 }]
	}, {
		supplierName : 'Ingrid',
		date : Date.now() + (1000 * 3600 * 24 * 3),
		items : [{ name : 'sutra', qty : 10, priceEach : 14000.00, totalPrice : 14000.00*10 }]
	}
]

transactionOut = [{
	transId : 'A23ae3',
	date : Date.now() + (1000 * 3600 * 24 * 3),
	items : [
		{ name : 'sutra', qty : 3, priceEach : 15000.00, totalPrice : 15000.00*3 },  
		{ name : 'extra joss', qty : 10, priceEach : 1500.00, totalPrice : 1500.00*10 }
	]
}, {
	transId : 'A23ae5',
	date : Date.now() + (1000 * 3600 * 24 * 4),
	items : [
		{ name : 'sutra', qty : 5, priceEach : 15000.00, totalPrice : 15000.00*5 }
	]
}]

var searchFor = 'sutra';
itemCollection = L(transactionIn).map(function (el) {
	return el.items.map(function (ele) {
		ele.supplierName = el.supplierName;
		ele.date = el.date;
		return ele;
	}); // inject supplierName and date to each item
}).reduce(function (a, b) {
	return a.concat(b);
});
itemCollection = L(itemCollection);
sutraQty = itemCollection.filter(function (el) {
	return el.name === searchFor;
}).map(function (el) {
	return el.priceEach;
}).uniq()
// ^ find all variation of priceEach, then find all the same price each in the combinedArr
// V then we map each unique priceEach their quantity
.map(function (ele) {
	var col = itemCollection.filter(function (el) {
		return el.priceEach === ele;
	});
	var sum = col.map(function (el) {
		return el.qty;
	}).reduce(function (a, b) {
		return a + b;
	});
	// for supplierNames
	var supplierNames = col.map(function (el) {
		return el.supplierName;
	}).uniq().toArray();
	// for dates
	var dates = col.map(function (el) {
		return el.date;
	}).uniq().toArray();
	return { priceEach : ele, totalQty : sum, supplierNames : supplierNames, dates : dates };
})
.toArray();

// console.log(sutraQty); // this will returns :
/*
[ { priceEach: 12000,
    totalQty: 16,
    supplierNames: [ 'John', 'Jane' ] },
  { priceEach: 14000, totalQty: 10, supplierNames: [ 'Ingrid' ] } ]
*/

var rawTable = L(transactionIn).map(function (el) {
	return el.items.map(function (ele) {
		ele.supplierName = el.supplierName;
		ele.date = el.date;
		return ele;
	}); // inject supplierName and date to each item
}).reduce(function (a, b) {
	return a.concat(b);
});
// console.log(rawTable); // this will returns :
/*
[ { name: 'sutra',
    qty: 10,
    priceEach: 12000,
    totalPrice: 120000,
    supplierName: 'John',
    date: 1405015215072 },
  { name: 'extra joss',
    qty: 30,
    priceEach: 1000,
    totalPrice: 30000,
    supplierName: 'John',
    date: 1405015215072 },
  { name: 'sutra',
    qty: 5,
    priceEach: 12000,
    totalPrice: 60000,
    supplierName: 'John',
    date: 1405188015072 },
  { name: 'sutra',
    qty: 1,
    priceEach: 12000,
    totalPrice: 12000,
    supplierName: 'Jane',
    date: 1405188015072 },
  { name: 'sutra',
    qty: 10,
    priceEach: 14000,
    totalPrice: 140000,
    supplierName: 'Ingrid',
    date: 1405274415072 } ]
// in which front end requested for table building
*/

// transform items into unique category [{ category : String, subCategories : [subCategory String] }, ...]
// var prev = null;
// var catSubcatArr = L(items).map(function (el) {
// 	var subcats = L(items).where({ category : el.category }).pluck('subCategory').toArray();
// 	return { category : el.category, subCategories : subcats };
// }).sortBy(function (el) {
// 	return el.category;
// }).map(function (el, i, arr) {
// 	if (!prev) {
// 		prev = el;
// 		return el;
// 	}
// 	if (prev.category !== el.category) {
// 		prev = el;
// 		return el;
// 	}
// }).filter(function (el) {
// 	if (el) {
// 		return true;
// 	}
// }).toArray();

// for convenience, filtering arr of object to be unique by key but you must sort it first
function uniqObj (arrIn, key) {
	var prev = null;
	return arrIn.map(function (el, i, arr) {
		if (!prev) {
			prev = el;
			return el;
		}
		if (prev[key] !== el[key]) {
			prev = el;
			return el;
		}
	}).filter(function (el) {
		if (el) {
			return true;
		}
	});
}

// console.log(catSubcatArr);

var newUpdateItems = [	
						{ name : 'sutra', qty : 1, priceEach : 15000.00, totalPrice : 15000.00*1 }, 
						{ name : 'kalpanax', qty : 2, priceEach : 3000, totalPrice : 3000*2 }
					]; 
// get all names from newUpdateItems and use .contains to check if el.name exists in the newUpdateItems
var newUpdateItemsNames = L(newUpdateItems).pluck('name');
var itemsToBeProcessed = L(transactionOut[0].items).map(function (el) {
	if (newUpdateItemsNames.contains(el.name)) { 
		// if el.name exists in newUpdateItemsNames, then calc the difference by substracting the old qty with the new qty of the same name
		// , else we simply return it
		var qty = el.qty - L(newUpdateItems).where({ name : el.name }).reduce(function (a, b) {
			return b.qty + a.qty;
		}, { qty : 0 });
		return { name : el.name, qty : qty };
	}
	return { name : el.name, qty : el.qty };
});
// this is to handle new element in newUpdateItems (in this case, kalpanax)
var itemsToBeProcessedNames = L(itemsToBeProcessed).pluck('name');
var newElements = L(newUpdateItems).map(function (el) {
	if (!itemsToBeProcessedNames.contains(el.name)) {
		return { name : el.name, qty : -1 * el.qty };
	}
}).compact();
itemsToBeProcessed = itemsToBeProcessed.concat(newElements).toArray();
console.log(itemsToBeProcessed); 
// Expecting
// [{ name : 'sutra', qty : 2 }, { name : 'extra joss', qty : 10 }, { name : 'kalpanax', qty : -2 }]
// we need to invert the qty of new item because we will substract current stock of the item with it (this is assuming transactionOut)
// if this is transactionOut, then increment stock if we got +qty, decrement if we got -qty (assuming we substract the old with the new)
// if this is transactionIn then increment stock if we got -qty, decrement if we got +qty (assuming we substract the old with the new)
// Consider the following for transactionOut:
// current stock 3
// old trans 2, which means current stock used to be 5
// new trans 1, the diff is +1 and current stock used to be higher, then current stock needs to be incremented
// Consider the following for transactionIn:
// current stock 3
// old trans 2, which means current stock used to be 1
// new trans 1, the diff is +1 and current stock used to be lower, then current stock needs to be decremented

process.exit();

// db.transactionIn.find({ $query : { items : { $elemMatch : { name : "Kondom Sutra" } } }, $orderBy : "date" })
// To find item in transactionIn by name ordered by date without .sort() method

// db.transactionIn.find({ items : { $elemMatch : { name : "Kondom Sutra" } } }).sort({ date : -1 }).limit(1)
// To find item in transactionIn by name ordered by date descending and limit to 1 aka the latest

// if itemAin123 == itemAin124 && buyPriceItemAin123 == buyPriceItemAin124 then sum qty