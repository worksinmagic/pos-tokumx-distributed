var configs = require('./configs.js');
var mongoskin = require('mongoskin');
var suspend = require('suspend');

var db = mongoskin.db(configs.dbUrl, { native_parser : true, w : 'majority', readPreference : 'nearest', forceServerObjectId : true, j : true });
db.bind('items');

run();

function run () {
	suspend.run(function * () {

		// get _id, name, sold, totalBought from items, sorted by name ascending
		var items = yield db.items.find({}, { _id : 1, name : 1, sold : 1, totalBought : 1 }).sort({ name : 1 }).toArray(suspend.resume());
		if (items.length > 0) {
			var scores = createScoresArr(items);
			var maxMinObj = createMaxMinObj(scores);
			var partitions = createRatingPartitions(maxMinObj, scores);
			var ratings = createFinalItemsRatingArr(partitions, scores);
			// update their ratings in db.items
			ratings.forEach(function (el) {
				db.items.update({ _id : mongoskin.helper.toObjectID(el._id) }, {
					$set : {
						rating : el.rating
					}
				}, suspend.fork());
			});
			yield suspend.join();

			console.log('Success updating rating in items collection');
		} else {
			console.log('Failed, no item found in database');
		}

		var now = Date.now();
		var nextHour = Math.ceil(now / (3600 * 1000)) * (3600 * 1000);
		var timeTillNextHour = nextHour - now;
		console.log('Waiting next run in ' + (timeTillNextHour / 1000) + ' seconds')
		yield setTimeout(suspend.resume(), timeTillNextHour);
		run();
	}, function (err) {
		if (err) {
			console.error(err);
		}
	});
}

function createScoresArr (arr) {
	var scores = arr.map(function (el) {
		el.score = el.sold / el.totalBought;
		( isNaN(el.score) || !isFinite(el.score) ) ? el.score = 0 : el.score = el.score;
		return el;
	});
	return scores;
}

function createMaxMinObj (arr) {
	var max = arr.sort(function (a, b) {
		return b.score - a.score;
	}).map(function (el) {
		return el.score;
	}).shift();
	var min = arr.sort(function (a, b) {
		return a.score - b.score;
	}).map(function (el) {
		return el.score;
	}).shift();
	return {
		max : max,
		min : min
	};
}

function createRatingPartitions (maxMinObj, arr) {
	var max = maxMinObj.max;
	var min = maxMinObj.min;
	var partition = (max - min) / arr.length;
	partition === 0 ? partition = 1 : partition = partition;
	var partitions = [];
	for (var i = 1; i <= 5; i++) {
		partitions.push({
			rating : i,
			limit : partition * i
		});
	}
	return partitions;
}

function createFinalItemsRatingArr (partitions, arr) {
	var ratings = arr.map(function (el) {
		partitions.forEach(function (ele) {
			if (el.score >= ele.limit) {
				el.rating = ele.rating;
			} else if (el.score < partitions[0].limit) {
				el.rating = 0;
			}
		});
		return el;
	});
	return ratings;
}