This is in database

```
	item = {
		type : 'item',
		name : string,
		category : string,
		subCategory : string,
		supplierName : string,
		barcode : [string],
		lastRestockDate : Date, -- no manual edit
		currentStock : number, -- no manual edit, will be checked by emailer periodically if got below minStock
		minStock : number,
		sold : number, -- incremented on output transaction based on qty in items
		totalBought : number, -- incremented on input transaction based on qty in items
		rating : number, -- no manual edit, will be setted by a service periodically based on transaction out
		amountType : 'piece'|'gram',
		bulkBuying : {
			qty : number,
			discount : number percentage
		},
		markUpFee : number percentage,
		handlingFee : number percentage
		GST : bool,
		picture : base64,
		bought : [
			{
				qty : number,
				price : number
			}, ...
		]
	}
	supplier = {
		name : string,
		address : string,
		phone : string,
		email : string
	}
	transaction = {
		type : 'input transaction' | 'output transaction'
		transId : shortid,
		buyer : 'employee' | 'customer', - only exists if type is 'output transaction'
		date : Date, - this is the new lastRestockDate for respective items if type is 'input transaction'
		items : [ - related items in this transaction
			{ 
				_id : hex of objectId of this item,
				supplierName : string,
				name : string, 
				qty : number, 
				priceEach : number, 
				totalPrice : number - 0 if buyer is 'employee'
			}, ... 
		]
		discount : number - Only on output transaction
	}
```

This is what should be sent

```
INSERTING

sock.emit('update output transaction', {
	query : { transId : '' },
	update : {
		buyer : 'customer',
		items : [
			{
				_id : '53cfe2ba7c35b0400fe7d1ad',
				supplierName : 'Matt & Co.',
				name : 'M&M',
				qty : 10,
				priceEach : 0.6,
				totalPrice : 0.6 * 10
			}
		]
	}
});

sock.emit('update input transaction', {
	query : { transId : '' },
	update : {
		type : 'input transaction',
		transId : '',
		items : [
			{ 
				_id : '53cfe2ba7c35b0400fe7d1ad',
				supplierName : 'Matt & Co.',
				name : 'M&M', 
				qty : 100, 
				priceEach : 0.5, 
				totalPrice : 0.5 * 100
			}
		]
	}
});

sock.emit('update item', { 
	query : { _id : '' },
	update : {
		type : 'item',
		name : 'M&M',
		category : 'foods',
		subCategory : 'snacks',
		supplierName : 'Matt & Co.',
		barcode : ['1938293049384'],
		minStock : 20,
		amountType : 'piece',
		bulkBuying : {
			qty : 20,
			discount : 0.1
		},
		markUpFee : 0.2,
		handlingFee : 0.1,
		GST : false,
		picture : ''
	}
});

sock.emit('update supplier', {
	query : { _id : '' },
	update :{
		name : 'Matt & Co.',
		address : 'New Orleans, LO',
		phone : '12223392',
		email : 'admin@mattand.co'
	},
	options : { upsert : true }
});
```