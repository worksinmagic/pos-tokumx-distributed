var configs = require('./configs.js'),
	axon = require('axon');

var frontEnd = axon.socket('rep'),
	backEnd = axon.socket('req');

frontEnd.bind(configs.axon.frontPort);
backEnd.bind(configs.axon.backPort);

frontEnd.on('message', function (msg, reply) {
	backEnd.send(msg, function (err, res) {
		reply(err, res);
	});
});

console.log('Broker server listening on port ' + configs.axon.frontPort + ' and ' + configs.axon.backPort);