/*
	For pos-hybrid-cluster, use Postgres as transaction server while TokuMX used as item and supplier server

	IN DEV MODE NOW, CHECK WORKER TO UNCOMMENT AUTH
	https://github.com/WebReflection/JSONH // use this to cut bandwidth usage >> JSONH.stringify and JSONH.parse
	User send the picture as base64

*/

// We serve the index.html and static files from nginx
var configs = require('./configs.js');

var http = require('http'),
	io = require('socket.io')(configs.axon.serverPort),
	fs = require('fs'),
	axon = require('axon');

// move this to worker after node 0.12 comes out
var shortid = require('shortid');

// io.serveClient(false);

var req = axon.socket('req');
req.set('hwm', configs.axon.hwm);
req.connect(configs.axon.frontPort);

// make the log files rotate using fs, change file path if process restart
// the convention is pos-log-N.log and pos-error-N.log
// var logFiles = fs.readdirSync('./logs'),
// 	errorFiles = fs.readdirSync('./errors'),
// 	newLogPath, newErrorPath;
// // get latest log files
// if (logFiles.length > 0 && errorFiles.length > 0) {
// 	var l = logFiles.map(function (el) { return parseInt(el.split('-')[2], 10) }).sort(function (a, b) { return b - a; })[0],
// 		e = logFiles.map(function (el) { return parseInt(el.split('-')[2], 10) }).sort(function (a, b) { return b - a; })[0];
// 	newLogPath = './logs/pos-log-'+(l++)+'.log';
// 	newErrorPath = './errors/pos-error-'+(e++)+'.log';
// } else {
// 	newLogPath = './logs/pos-log-0.log';
// 	newErrorPath = './errors/pos-error-0.log';
// }

// require('logbook').configure({
// 	console : {
// 		enabled : false
// 	},
// 	file : {
// 		enabled : true,
// 		log : true,
// 		error : true,
// 		logPath : newLogPath,
// 		errPath : newErrorPath
// 	}
// });

req.on('drop', function (msg) {
	io.emit('server busy', 'Your message has been dropped by server because the server is busy, please try again later');
});

io.on('connection', function (socket) {
	socket.on('error', function (err) {

	});
	socket.on('disconnect', function () {

	});
	socket.on('login', function (msg) {
		// msg.route = 'login';
		// req.send(msg, function (err, res) {
		// 	if (err) {
		// 		if (err.raw) {
		// 			console.error(err.raw);
		// 			delete err.raw;
		// 		}
		// 		socket.emit('login listener', err);
		// 		return;
		// 	}
		// 	socket.emit('login listener', res);
		// });
	});
	socket.on('register', function (msg) {

	});
	socket.on('fetch items by query', function (msg) {
		msg.route = 'fetch items by query';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('FETCH ITEMS BY QUERY - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('fetch items by query listener', err);
				return;
			}
			socket.emit('fetch items by query listener', res);
		});
	});
	socket.on('fetch input transactions by query', function (msg) {
		msg.route = 'fetch input transactions by query';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('FETCH INPUT TRANSACTIONS BY QUERY - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('fetch input transactions by query listener', err);
				return;
			}
			socket.emit('fetch input transactions by query listener', res);
		});
	});
	socket.on('fetch output transactions by query', function (msg) {
		msg.route = 'fetch output transactions by query';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('FETCH OUTPUT TRANSACTIONS BY QUERY - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('fetch output transactions by query listener', err);
				return;
			}
			socket.emit('fetch output transactions by query listener', res);
		});
	});
	socket.on('fetch suppliers by query', function (msg) {
		msg.route = 'fetch suppliers by query';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('FETCH SUPPLIER BY QUERY - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('fetch suppliers by query listener', err);
				return;
			}
			socket.emit('fetch suppliers by query listener', res);
		});
	});
	socket.on('update item', function (msg) {
		if (msg.query && typeof msg.query._id === 'string') {
			if (msg.query._id.length === 0) {
				msg.flag = 'insert';
			} else {
				msg.flag = 'update';
			}
		} else {
			socket.emit('update item listener', { error : 'ERROR : Invalid query, no _id specified' });
		}
		msg.route = 'update item';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('UPDATE ITEM - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('update item listener', err);
			}
			socket.emit('update item listener', res);
		});
	});
	socket.on('delete item', function (msg) {
		msg.flag = 'delete';
		msg.route = 'update item';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('DELETE ITEM - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('delete item listener', err);
			}
			socket.emit('delete item listener', res);
		});
	});
	socket.on('update input transaction', function (msg) {
		if (msg.query && typeof msg.query.transId === 'string') {
			if (msg.query.transId.length === 0) {
				msg.flag = 'insert';
				// THIS IS A HACK, MOVE THIS TO WORKER AFTER NODE v0.12 COMES OUT!!!
				msg.update.transId = shortid.generate();
				msg.query.transId = msg.update.transId;
				// HACK ENDED HERE
			} else {
				if (msg.options !== 'delete') {
					msg.flag = 'update';
				} else {
					msg.flag = 'delete';
				}
			}
		} else {
			socket.emit('update input transaction listener', { error : 'ERROR : Invalid query, no transId specified' });
			return;
		}
		msg.route = 'update input transaction';
		msg.update.type = 'input transaction';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('UPDATE INPUT TRANSACTION - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('update input transaction listener', err);
			}
			socket.emit('update input transaction listener', res);
		});
	});
	socket.on('update output transaction', function (msg) {
		// THIS IS A HACK, MOVE THIS TO WORKER AFTER NODE v0.12 COMES OUT!!!
		if (msg.query && typeof msg.query.transId === 'string') {
			if (msg.query.transId.length === 0) {
				msg.flag = 'insert';
				msg.update.transId = shortid.generate();
				msg.query.transId = msg.update.transId;
			} else {
				if (msg.options !== 'delete') {
					msg.flag = 'update';
				} else {
					msg.flag = 'delete';
				}
			}
		} else {
			// should be reply(err, null) here if in worker
			socket.emit('update output transaction listener', { error : 'ERROR : Invalid query, no transId specified' });
			return;
		}
		// HACK ENDED HERE
		msg.route = 'update output transaction';
		msg.update.type = 'output transaction';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('UPDATE OUTPUT TRANSACTION - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('update output transaction listener', err);
			}
			socket.emit('update output transaction listener', res);
		});
	});
	socket.on('update supplier', function (msg) {
		msg.route = 'update supplier';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('UPDATE SUPPLIER - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('update supplier listener', err);
			}
			socket.emit('update supplier listener', res);
		});
	});
	socket.on('delete supplier', function (msg) {
		msg.route = 'update supplier';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('DELETE SUPPLIER - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('delete supplier listener', err);
			}
			socket.emit('delete supplier listener', res);
		});
	});
	socket.on('get depleted items', function (msg) {
		msg.route = 'get depleted items';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('GET DEPLETED ITEMS - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('get depleted items listener', err);
			}
			socket.emit('get depleted items listener', res);
		});
	});
	socket.on('send email', function (msg) {
		msg.route = 'send email';
		req.send(msg, function (err, res) {
			if (err) {
				if (err.raw) {
					console.error('SEND EMAIL - ERROR : ', err.raw);
					delete err.raw;
				}
				socket.emit('send email listener', err);
			}
			socket.emit('send email listener', res);
		});
	});
	socket.on('send display', function (msg) { // display listen to 'receive display' broadcast
		socket.broadcast.emit('receive display', msg);
	});
	socket.on('admin broadcast notification', function (msg) {
		// msg.route = 'admin broadcast notification';
		// req.send(msg, function (err, res) {
		// 	if (err) {
		// 		if (err.raw) {
		// 			console.error(err.raw);
		// 			delete err.raw;
		// 		}
		// 		socket.emit('admin broadcast notification listener', err);
		// 		return;
		// 	}
		// 	socket.broadcast.emit('admin notification', { msg : res.msg });
		// 	socket.emit('admin broadcast notification listener', { error : res.error });
		// });
	});
	socket.on('ping', function (msg) {
		socket.emit('pong', 'PONG');
	});
});

console.log('Socket.io listening on '+configs.axon.serverPort);