// FOR ADDED SECURITY, LOG USERNAME, USERIP, AND ACCESS DATE IF UPDATING, probably by appending lastModified : { username : , ip : , date : } to the document
// and omit them if showing to users. Create new collection 'accessLog' for the logs whether updating or querying 
// { username : , ip : , date : , command : JSON.stringify(theirCommand) }

// FLAW IN DB STRUCTURE, RESTRUCTURE THE DB FIRST, DENORMALIZE LIKE HELL
// move stock and bought from items to transaction, remove lastRestockDate from items and just query the latest transactionIn that includes the item, 
// remove transactionIn and transactionOut and replace with just transaction

// the msg.flag is awful and not necessary, might be better if we use msg.route instead like 'insert input transaction'
// maybe do this for app.js too


var configs = require('./configs.js')//,
	// dbChecker = require('./dbChecker.js');

var	mongoskin = require('mongoskin'),
	mandrill = require('mandrill-api/mandrill'),
	suspend = require('suspend'),
	axon = require('axon'),
	L = require('lazy.js');

var mailClient = new mandrill.Mandrill(configs.mandrill.key);

var worker = axon.socket('rep');
worker.connect(configs.axon.backPort);

// db setup, this is for read only or not necessarily must consistent read content
var db = mongoskin.db(configs.dbUrl, { native_parser : true, w : '0', readPreference : 'nearest', forceServerObjectId : true, j : false });
db.bind('items');
db.bind('transactions');
db.bind('suppliers');

// this is configured for speed with writeConcern 1 alias primary only and readPreference from the nearest server
var dbs = mongoskin.db(configs.dbUrl, { native_parser : true, w : 1, readPreference : 'nearest', forceServerObjectId : true, j : false });
dbs.bind('sessions');
dbs.sessions.ensureIndex({ createdAt : 1 }, { expireAfterSeconds : 3600 * 24 }, function (err) {
	if (err) {
		console.error(err);
		process.exit();
	}
});
// session expired after 24 hours, or use localStorage on client to store token and remove session expiration
// with this setting, we need to renew token every db access

// for best speed, use SSD at RAID 10 or RAID 5 or RAID 6 because waiting for journal write means we are limited to disk speed.
// we use third client for consistency on transaction with read using readPreference primary
var dbx = mongoskin.db(configs.dbUrl, { native_parser : true, w : 'majority', readPreference : 'primary', forceServerObjectId : true, j : true });
dbx.bind('items');
dbx.bind('suppliers');

// for class checking
// Object.prototype.toString.call(yourItem)

worker.on('message', function (msg, reply) {
	suspend.run(function * () {
		// var dbIsConnected = dbChecker.isDbConnected();
		// if (!dbIsConnected) {
		// 	reply({ error : 'Db is not ready, no primary server exists' }, null);
		// 	return;
		// }
		// if (msg.route === 'login') {
			// do login here
			// return;
		// }
		// check token
		// var user = yield dbs.sessions.findOne({ token : msg.token }, suspend.resume());
		// if (!user) {
		// 	reply({ error : 'Invalid token' }, null);
		// 	return;
		// }
		// token valid, resuming
		// update token expiry
		// yield dbs.sessions.update({ token : msg.token }, { $set : { createdAt : new Date() } }, suspend.resume());
		// send the items
		switch (msg.route) {
			case 'register' :
				// to be developed
			break;

			case 'login' :
				// to be developed
			break;

			case 'fetch items by query' :
				var itemsDesc = yield db.items.find(msg.query, msg.options).limit(msg.limit).toArray(suspend.resume());
				reply(null, { error : false, items : itemsDesc });
			break;

			case 'fetch input transactions by query' :
				// Date-ified date query
				if (msg.query.date && Object.prototype.toString.call(msg.query.date) === '[object Object]') {
					if (Object.keys(msg.query.date).length > 0) {
						for (var k in msg.query.date) {
							msg.query.date[k] = new Date(msg.query.date[k]);
						}
					} else {
						msg.query.date = new Date(msg.query.date);
					}
				}
				msg.query.type = 'input transaction';
				//
				var transactionIn = yield db.transactions.find(msg.query, msg.options).limit(msg.limit).toArray(suspend.resume())
				//
				reply(null, { error : false, transactionIn : transactionIn });
			break;

			case 'fetch output transactions by query' :
				if (msg.query.date && Object.prototype.toString.call(msg.query.date) === '[object Object]') {
					if (Object.keys(msg.query.date).length > 0) {
						for (var k in msg.query.date) {
							msg.query.date[k] = new Date(msg.query.date[k]);
						}
					} else {
						msg.query.date = new Date(msg.query.date);
					}
				}
				msg.query.type = 'output transaction';
				//
				var transactionOut = yield db.transactions.find(msg.query, msg.options).limit(msg.limit).toArray(suspend.resume());
				//
				reply(null, { error : false, transactionOut : transactionOut });
			break;

			case 'fetch suppliers by query' :
				var suppliers = yield db.suppliers.find(msg.query, msg.options).limit(msg.limit).toArray(suspend.resume());
				reply(null, { error : false, suppliers : suppliers });
			break;

			case 'update item' :
				var result;
				if (msg.flag === 'insert') {

					msg.update.sold = 0;
					msg.update.totalBought = 0;
					msg.update.rating = 0;
					msg.update.currentStock = 0;
					result = yield dbx.items.insert(msg.update, suspend.resume());
					
				} else if (msg.flag === 'update') {
					result = yield dbx.items.updateById(msg.query._id, msg.update, msg.options, suspend.resume());
				} else if (msg.flag === 'delete') {
					result = dbx.items.removeById(msg.query._id, suspend.resume());
				}
				reply(null, { result : result });
			break;

			case 'update input transaction' :
				if (msg.flag === 'insert') {

					// BUG!!
					// WHAT IF THERE ARE SAME ITEM WITH DIFFERENT PRICE AND SUPPLIERNAME ON ONE TRANSACTION? -- fixed applied, tested well

					msg.update.date = new Date();

					var result = yield dbx.eval(function (update) {
						// edit lastRestockDate, currentStock, totalBought, and bought (if new priceEach)
						update.items.forEach(function (el) {
							db.items.update({ _id : ObjectId(el._id) }, {
								$set : {
									lastRestockDate : update.date
								},
								$inc : {
									currentStock : el.qty,
									totalBought : el.qty
								}
							});
							// check the bought
							var bought = db.items.findOne({ _id : ObjectId(el._id) }, { bought : 1 }).bought;
							// if bought not exists, then we set new bought
							if (!bought) {
								db.items.update({ _id : ObjectId(el._id) }, {
									$set : {
										bought : [{
											price : el.priceEach,
											qty : el.qty
										}]
									}
								});
							} else {
								// bought exists, match it with el.priceEach
								// if price === priceEach
								// then we update the qty
								// otherwise, we just push
								db.items.update({
									_id : ObjectId(el._id),
									'bought.price' : el.priceEach
								}, {
									$inc : {
										'bought.$.qty' : el.qty
									}
								});
								db.items.update({
									_id : ObjectId(el._id),
									'bought.price' : {
										$ne : el.priceEach
									}
								}, {
									$push : {
										bought : {
											qty : el.qty,
											price : el.priceEach
										}
									}
								});
							}
						});
						// and put the transaction into db
						db.transactions.insert(update);
						return 'Success inserting new input transaction';
					}, msg.update, suspend.resume());

					reply(null, { error : false, result : result, transId : msg.update.transId });

				} else if (msg.flag === 'update') {

					// BUG : even if an item in items is deleted, the bought will stay -- fixed applied , tested well
					msg.update.date = new Date(msg.update.date);

					var result = yield dbx.eval(function (update) {
						// WHAT IF THERE ARE SAME ITEM WITH DIFFERENT PRICE ON ONE TRANSACTION?
						// get target transaction
						var targetTransaction = db.transactions.findOne({ transId : update.transId });
						// revert all the items' lastRestockDate, totalBought and currentStock, including bought
						// get the item's last input transaction date and apply it
						targetTransaction.items.forEach(function (el) {
							var lastItemInputTransDate = db.transactions.find({ 'items._id' : el._id }, { date : 1 }).sort({ date : -1 }).limit(1)[0].date;
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$set : {
									lastRestockDate : lastItemInputTransDate
								},
								$inc : {
									totalBought : -el.qty,
									currentStock : -el.qty
								}
							});
							// revert bought
							db.items.update({
								_id : ObjectId(el._id),
								'bought.price' : el.priceEach
							}, {
								$inc : {
									'bought.$.qty' : -el.qty
								}
							});
							// if the bought is 0 qty, remove it
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$pull : {
									bought : {
										qty : 0
									}
								}
							});
						});
						// apply new items' totalBought, currentStock, and bought
						update.items.forEach(function (el) {
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$set : {
									lastRestockDate : update.date
								},
								$inc : {
									totalBought : el.qty,
									currentStock : el.qty
								}
							});
							// now the bought
							var bought = db.items.findOne({ _id : ObjectId(el._id) }, { bought : 1 }).bought;
							// if bought not exists, then we set new bought
							if (!bought || bought.length === 0) {
								db.items.update({ _id : ObjectId(el._id) }, {
									$set : {
										bought : [{
											price : el.priceEach,
											qty : el.qty
										}]
									}
								});
							} else {
								// bought exists, match it with el.priceEach
								// if price === priceEach
								// then we update the qty
								// otherwise, we just push
								db.items.update({
									_id : ObjectId(el._id),
									'bought.price' : el.priceEach
								}, {
									$inc : {
										'bought.$.qty' : el.qty
									}
								});
								db.items.update({
									_id : ObjectId(el._id),
									'bought.price' : {
										$ne : el.priceEach
									}
								}, {
									$push : {
										bought : {
											qty : el.qty,
											price : el.priceEach
										}
									}
								});
							}
						});
						// and update the target transaction
						db.transactions.update({ transId : update.transId }, {
							$set : {
								date : update.date,
								items : update.items
							}
						});
						return 'Success updating input transaction';
					}, msg.update, suspend.resume());

					reply(null, { error : false, result : result });

				} else if (msg.flag === 'delete') {

					// UNTESTED
					var result = yield dbx.eval(function (transId) {
						// get target transaction
						var targetTransaction = db.transactions.findOne({ transId : transId });
						// revert currentStock and totalBought and lastRestockDate
						targetTransaction.items.forEach(function (el) {
							var lastItemInputTransDate = db.transactions.find({ 'items._id' : el._id }, { date : 1 }).sort({ date : -1 }).limit(1)[0].date;
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$set : {
									lastRestockDate : lastItemInputTransDate
								},
								$inc : {
									currentStock : -el.qty,
									totalBought : -el.qty
								}
							});
							// revert bought
							db.items.update({
								_id : ObjectId(el._id),
								'bought.price' : el.priceEach
							}, {
								$inc : {
									'bought.$.qty' : -el.qty
								}
							});
							// if there are any 0 qty, remove them
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$pull : {
									bought : {
										qty : 0
									}
								}
							});
						});


						// finally, remove targetTransaction
						db.transaction.remove({ transId : transId });
						return 'Success deleting input transaction';
					}, msg.query.transId, suspend.resume());

					reply(null, { error : false, result : result });

				}
			break;

			case 'update output transaction' :
				if (msg.flag === 'insert') { // for inserting, user did not have to send options anymore, it will use insert instead

					msg.update.date = new Date();
					// ACID but slow because global lock, good enough for now but won't work on sharded cluster
					var result = yield dbx.eval(function (update) {
						// increment sold and decrement currentStock
						update.items.forEach(function (el) {
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$inc : {
									currentStock : -el.qty,
									sold : el.qty
								}
							});
						});
						// put the transaction to db
						db.transactions.insert(update);
						return 'Success inserting new output transaction';
					}, msg.update, suspend.resume());
					reply(null, { error : false, result : result, transId : msg.update.transId });

				} else if (msg.flag === 'update') { // for updating

					msg.update.date = new Date(msg.update.date);

					// we need to edit sold and currentStock 
					var result = yield dbx.eval(function (update) {
						// get target transaction
						var targetTransaction = db.transactions.findOne({ transId : update.transId });
						// revert sold and currentStock
						targetTransaction.items.forEach(function (el) {
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$inc : {
									currentStock : el.qty,
									sold : -el.qty
								}
							});
						});
						// apply the new transaction.item
						update.items.forEach(function (el) {
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$inc : {
									currentStock : -el.qty,
									sold : el.qty
								}
							});
						});
						// now update the transaction
						db.transactions.update({ transId : update.transId }, {
							$set : {
								buyer : update.buyer,
								date : update.date,
								items : update.items,
								discount : update.discount
							}
						});
						return 'Success updating output transaction';

					}, msg.update, suspend.resume());

					reply(null, { error : false, result : result });
					
				} else if (msg.flag === 'delete') {

					var result = yield dbx.eval(function (transId) {
						// get target transaction
						var targetTransaction = db.transactions.findOne({ transId : transId });
						// revert sold and currentStock
						targetTransaction.items.forEach(function (el) {
							db.items.update({
								_id : ObjectId(el._id)
							}, {
								$inc : {
									sold : -el.qty,
									currentStock : -el.qty
								}
							});
						});
						// remove target transaction
						db.transactions.remove({ transId : transId });
						return 'Success deleting output transaction';
					}, msg.query.transId, suspend.resume());
					reply(null, { error : false, result : result });

				}
			break;

			case 'update supplier' :
				if (msg.flag !== 'delete') {
					// put in db
					// BUG : { upsert : true } somehow makes it an insert even if query return item
					var result = yield dbx.suppliers.updateById(msg.query._id, msg.update, msg.options, suspend.resume());
				} else {
					var result = yield dbx.suppliers.removeById(msg.query._id, suspend.resume());
				}
				reply(null, { error : false, result : result });
			break;

			case 'get depleted items' :
				var depletedItems = yield db.items.find({ $where : 'this.currentStock < this.minStock' }).toArray(suspend.resume());
				if (depletedItems.length > 0) {
					depletedItems.forEach(function (el) {
						db.suppliers.findOne({ name : el.supplierName }, suspend.fork());
					});
					var suppliers = yield suspend.join();
					depletedItems = depletedItems.map(function (el, i) {
						el.supplierEmail = suppliers[i].email;
						el.supplierAddress = suppliers[i].address;
						el.supplierPhone = suppliers[i].phone;
						return el;
					});
				}
				reply(null, { error : false, depletedItems : depletedItems });
			break;

			case 'send email' :
				var message = {
					text : msg.text,
					subject : msg.subject,
					from_email : configs.mandrill.fromEmail,
					from_name : configs.mandrill.fromName,
					to : [{
						email : msg.supplierEmail,
						name : msg.supplierName,
						type : 'to'
					}],
					important : true
				}
				mailClient.messages.send({
					message : message,
					async : true,
				}, function (res) {
					reply(null, { error : false, result : res });
				}, function (err) {
					reply({ error : 'Mail sending error', raw : err }, null);
				});
			break;

			case 'admin broadcast notification' :
				// to be developed
			break;

		}
	}, function (err) {
		if (err) {
			console.error(err);
			reply({ error : 'Database error', raw : err.toString() }, null);
		}
	});
});

console.log('Worker listening port ' + configs.axon.backPort);