exports.isDbConnected = function () {
	var result = yield db.admin().replSetGetStatus(suspend.resume());
	var primaryExists = false,
		reachableServers = 0;

	result.members.forEach(function (el, i, arr) {
		if (el.health == 1) {
			reachableServers++;
		}
		if (el.stateStr == 'PRIMARY') {
			primaryExists = true;
		}
	});
	if (reachableServers < result.members.length && !primaryExists) {
		return false;
	}
	// all is well, proceed
	return true;
}