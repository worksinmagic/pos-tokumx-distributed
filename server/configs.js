module.exports = {
	dbUrl : 'mongodb://localhost:27017/pos',
	axon : {
		serverPort : 7777,
		frontPort : 30000,
		backPort : 30001,
		display : 30002,
		hwm : 10000 // 10000 messages in queue before dropping messages, probably only matter in 'req' and 'push'
	},
	mandrill : {
		key : 'Sz58-qrLJJNIKOf-f91p1Q',
		// key : 'xuMIOgeudOKI6VaWBOmO-g',
		fromEmail : 'no-reply@worksinmagic.com',
		fromName : 'Asian Provisions'
	}
};