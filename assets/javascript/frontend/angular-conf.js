var query = {
	query: {},
	options: {}
};

var queryTransactionOutController = {
	query: {
		// $where: "this.barcode.length == 0"
	},
	options: {
		stock: 0,
		minStock: 0,
		supplier: 0,
		lastRestockDate: 0,
		rating: 0,
		bought: 0,
		barcode: 0,
		markUpFee: 0,
		GST: 0,
		handlingFee: 0,
		amountType: 0
	}
};

var app = angular.module('posApp', ['ui.router', 'ui.unique', 'ui.bootstrap', 'ngAnimate', 'btford.socket-io', 'ngSanitize']);
var fileInput = app.directive('fileInput', function($parse) {
	return {
		restrict: 'EA',
		template: '<input type="file" class="form-control" />',
		replace: true,
		link: function(scope, element, attrs) {
			var modelGet = $parse(attrs.fileInput);
			var modelSet = modelGet.assign;
			var onChange = $parse(attrs.onChange);

			var updateModel = function() {
				scope.$apply(function() {
					modelSet(scope, element[0].files[0]);
					onChange(scope);
				});
			};

			element.bind('change', updateModel);
		}
	};
});
var setFocus = app.directive('setFocus', function() {
	return {
		restrict: 'EA',
		link: function( scope, element, attr ) {
			scope.$watch(attr.setFocus, function(n, o) {
				if(n != 0 && n) {
					element[0].focus();
				};
			})
		}
	}
});
var appsocket =	app.factory('socket', function(socketFactory) {
	return socketFactory({
		ioSocket: io.connect('http://localhost:7777/')
	});
});

(function (module) {
     
    var fileReader = function ($q, $log) {
 
        var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
 
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
 
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
 
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);        
            reader.readAsDataURL(file);
             
            return deferred.promise;
        };
 
        return {
            readAsDataURL: readAsDataURL 
        };
    };
 
    module.factory("fileReader",
                   ["$q", "$log", fileReader]);
 
}(angular.module("posApp")));

appsocket.controller('transactionOutController', function(socket, $scope, $stateParams) {
	var lists = [];
	var _price = [];
	$scope.discount = 0;
	$scope.focusBarcode = true;
	$scope.header = new Date();
	$scope.cash = 0;
	$scope.change = parseFloat(0);
	socket.on('fetch items by query listener', function(data) {
		if(!data.error) {
			if(data.items.length > 1) {
				$scope.items = data.items;
				$scope.search = { name: '' };
			} else {
				if(data.items.length == 1) {
					if($scope.qty > 0) {
						var qty = parseFloat($scope.qty);
						$scope.errorMessage = '';
					} else if($scope.qty === 0 || $scope.qty == null) {
						var qty = parseFloat(1);
						$scope.errorMessage = '';
					} else if($scope.qty < 0) {
						var qty = NaN;
						$scope.errorMessage = 'Quantity is negative. Please re-check.';
					}
					$scope.currentStock = data.items[0].currentStock;
					if(qty <= parseFloat($scope.currentStock)) {
						lists.push({
							_id: data.items[0]._id,
							name: data.items[0].name,
							barcode: data.items[0].barcode,
							currentStock: data.items[0].currentStock,
							qty: parseFloat(qty),
							bought: _price,
							markUpFee: data.items[0].markUpFee,
							handlingFee: data.items[0].handlingFee,
							GST: data.items[0].GST,
							amountType: data.items[0].amountType,
							bulkBuying: data.items[0].bulkBuying,
							supplierName: data.items[0].supplierName,
							priceEach: []
						});
						$scope.errorMessage = '';
						_price = [];
					} else {
						$scope.errorMessage = 'Quantity is exceeding current stock. Please re-check.';
					}
					$scope.lists = lists;
					$scope.qty = null; // It needs to be here since I can't set it to null on upsertLists before I get the value to be calculated here
				} else {
					$scope.error = 'has-error';
				}
			}
		}
	});
	socket.on('update output transaction listener', function(data) {
		console.log(data);
	});
	socket.on('fetch output transactions by query listener', function(data) {
		if(!data.error) {
			if(data.transactionOut.length != 0) {
				console.log(JSON.stringify(data));
				lists = [];
				$scope.task = '- Editing transaction: ' + data.transactionOut[0].transId;
				$scope.transDate = data.transactionOut[0].date;
				Lazy(data.transactionOut[0].items).each( function(el) {
					lists.push({
						_id: el._id,
						name: el.name,
						barcode: el.barcode,
						qty: parseFloat(el.qty),
						currentStock: el.currentStock,
						bought: el.bought,
						markUpFee: el.markUpFee,
						handlingFee: el.handlingFee,
						GST: el.GST,
						amountType: el.amountType,
						bulkBuying: el.bulkBuying,
						supplierName: el.supplierName
					});
				});
				console.log(JSON.stringify(lists));
				$scope.lists = lists;
			} else {
				$scope.task = '- No transaction found';
			}
		}
	});
	socket.on('fetch input transactions by query listener', function(data) {
		if(!data.error) {
			// Lazy(lists).each( function(el) {
			// 	Lazy(data.transactionIn[0].items).each( function(ul) {
			// 		if(ul._id === el._id) {
			// 			console.log(el);
			// 			el.priceEach.push(ul.priceEach);
			// 		}
			// 	});
			// });
			Lazy(data.transactionIn).each( function(el) {
				Lazy(el.items).each( function(ul) {
					var haveBarcode = Lazy(ul.barcode).contains($scope.barcode);
					if(ul.name === $scope.search.name || haveBarcode) {
						_price.push(ul.priceEach);
					}
				});
			});
			// console.log(data);
			console.log(_price);
			var options = {
					stock: 0,
					minStock: 0,
					supplier: 0,
					lastRestockDate: 0,
					rating: 0,
					category: 0,
					subCategory: 0,
					picture: 0
				};
			if( $scope.barcode == '' || $scope.barcode == null ) {
				var querySingleItem = {
					query: {
						name: $scope.search.name
					},
					options: options
				}
			} else {
				var querySingleItem = {
					query: {
						barcode: $scope.barcode
					},
					options: options
				}
			}
			socket.emit('fetch items by query', querySingleItem);
			$scope.search.name = '';
			$scope.barcode = '';
			$scope.focusBarcode = true;
		}
	});
	$scope.changeCategory = function( category ) {
		$scope.category = category;
		$scope.subCategory = '';
	};
	$scope.changeSubCategory = function( subCategory ) {
		$scope.subCategory = subCategory;
	};
	$scope.sendToEditor = function(name, qty, barcode) {
		$scope.search.name = name;
		$scope.barcode = barcode != null ? barcode : null;
		$scope.qty = qty;
	};
	$scope.upsertLists = function() {
		var found = false;
		Lazy(lists).each( function(el) {
			console.log(lists);
			var haveBarcode = Lazy(el.barcode).contains($scope.barcode);
			// var qty = $scope.qty > 0 ? $scope.qty : parseInt(el.qty, 10) + parseInt(1, 10);
			if($scope.qty > 0) {
				var qty = parseFloat(el.qty) + parseFloat($scope.qty);
				$scope.errorMessage = '';
			} else if($scope.qty === 0 || $scope.qty == null) {
				var qty = parseFloat(el.qty) + parseFloat(1);
				$scope.errorMessage = '';
			} else if($scope.qty < 0) {
				var qty = parseFloat(el.qty);
				$scope.errorMessage = 'Quantity is negative. Please re-check.';
			}
			if( (haveBarcode && el.name == $scope.search.name) || (haveBarcode) || (!haveBarcode && el.name == $scope.search.name)  ) {
				if(qty <= el.currentStock) {
					el.qty = qty;
					found = true;
					$scope.errorMessage = '';
				} else {
					$scope.errorMessage = 'Quantity is exceeding current stock. Please re-check.';
					found = true;
				}
			}
		});
		if(!found) {
			if( $scope.barcode == '' || $scope.barcode == null ) {
				var queryTransactionInLists = {
					query: {
						$query: {
							items: {
								$elemMatch: { name: $scope.search.name }
							},
							type: 'input transaction'
						},
						$orderby: {
							date: -1
						}
					},
					options: {},
					limit: 3
				};
			} else {
				var queryTransactionInLists = {
					query: {
						$query: {
							items: {
								$elemMatch: { barcode: $scope.barcode }
							},
							type: 'input transaction'
						},
						$orderBy: {
							date: -1
						}
					},
					options: {},
					limit: 3
				};
			}
			console.log(JSON.stringify(queryTransactionInLists));
			socket.emit('fetch input transactions by query', queryTransactionInLists);
		}
	};
	$scope.getPrice = function(price, markup, handling, qty, bulk) {
		var priceUsed;
		Lazy(price).each( function(el) {
			priceUsed = priceUsed > el ? priceUsed : el;
		});
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(markup) / 100);
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(handling) / 100);
		if(parseFloat(qty) >= parseFloat(bulk.qty)) {
			priceUsed = parseFloat(priceUsed) - (parseFloat(priceUsed) * parseFloat(bulk.discount) / 100);
		}
		priceUsed = priceUsed.toFixed(3).split('.');
		priceUsed[1] = priceUsed[1].slice(0,2);
		priceUsed = priceUsed.join('.');
		return priceUsed;
	};
	$scope.getTotal = function(name, price, markup, handling, qty, bulk) {
		var priceUsed;
		var priceEach;
		Lazy(price).each( function(el) {
			priceUsed = priceUsed > el ? priceUsed: el;
		});
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(markup) / 100);
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(handling) / 100);
		if(qty >= bulk.qty) {
			priceUsed = parseFloat(priceUsed) - (parseFloat(priceUsed) * parseFloat(bulk.discount) / 100);
		}
		priceUsed = priceUsed.toFixed(3).split('.');
		priceUsed[1] = priceUsed[1].slice(0,2);
		priceUsed = priceUsed.join('.');
		priceEach = priceUsed;
		priceUsed = priceUsed * qty;
		priceUsed = priceUsed.toFixed(2);
		Lazy($scope.lists).each( function(el) {
			if(el.name == name) {
				el.priceEach = priceEach;
				el.totalAll = priceUsed;
			}
		});
		var msgDisplay = {
			type: 'lists',
			content: lists
		}
		socket.emit('send display', msgDisplay);
		return priceUsed;
	};
	$scope.getTotalAll = function() {
		var sumAll = 0;
		sumAll = parseFloat(sumAll);
		Lazy($scope.lists).each( function(el) {
			sumAll += parseFloat(el.totalAll);
		});
		if(!isNaN(sumAll)) {
			sumAll = sumAll.toFixed(3).split('.');
			sumAll[1] = sumAll[1].slice(0,2);
			sumAll = sumAll.join('.');
		}
		var thatFinalPrice = parseFloat(sumAll) - (parseFloat(sumAll) * parseFloat($scope.discount == '' ? 0 : $scope.discount) / 100);
		if(!isNaN(thatFinalPrice)) {	
			thatFinalPrice = thatFinalPrice.toFixed(3).split('.');
			thatFinalPrice[1] = thatFinalPrice[1].slice(0,2);
			thatFinalPrice = thatFinalPrice.join('.');
		}
		$scope.finalPrice = thatFinalPrice;
		var msgDisplay = {
			type: 'total',
			content: {
				sumAll: sumAll,
				finalPrice: $scope.finalPrice
			}
		};
		socket.emit('send display', msgDisplay);
		return sumAll;
	};
	$scope.upsertTransactionOut = function() {
		var dbUpsertItems = [];
		$scope.lists = angular.fromJson(angular.toJson($scope.lists));
		Lazy($scope.lists).each( function(el) {
			dbUpsertItems.push({
				_id: el._id,
				date: el.date,
				name: el.name,
				qty: el.qty,
				bought: el.bought,
				barcode: el.barcode,
				priceEach: el.priceEach,
				totalPrice: el.totalAll,
				supplierName: el.supplierName,
				markUpFee: el.markUpFee,
				amountType: el.amountType,
				bulkBuying: el.bulkBuying,
				handlingFee: el.handlingFee
			});
		});
		var query = {
			query: {
				transId: $stateParams.id == null ? '' : $stateParams.id
			},
			update: {
				transId: $stateParams.id == null ? '' : $stateParams.id,
				buyer: parseFloat($scope.discount) == 100 ? 'employee' : 'customer',
				items: dbUpsertItems,
				discount: $scope.discount == '' ? 0 : $scope.discount,
				date: $scope.transDate
			}
		};
		// console.log(JSON.stringify(query));
		socket.emit('update output transaction', query);
	};
	$scope.clearThisTransaction = function() {
		$scope.lists = [];
		lists = [];
		$scope.barcode = '';
		$scope.search.name = '';
		$scope.qty = '';
	};
	$scope.fixNaN = function() {
		if(isNaN($scope.discount)) {
			$scope.discount = 0;
		}
		var msgDisplay = {
			type: 'discount',
			content: $scope.discount
		};
		socket.emit('send display', msgDisplay);
	};
	$scope.deleteArr = function(list) {
		var indexOf = $scope.lists.indexOf(list);
		indexOf > -1 ? $scope.lists.splice(indexOf, 1) : '';
		console.log(lists);
	};
	$scope.updateChange = function() {
		var cash = 0;
		var change = 0;
		if($scope.cash == null || $scope.cash == '') {
			cash = parseFloat(0);
		} else if(isNaN($scope.cash) || isNaN(parseFloat($scope.cash))) {
			$scope.cash = parseFloat(0);
			cash = parseFloat($scope.cash);
		} else {
			cash = parseFloat($scope.cash);
			change = parseFloat(cash) - parseFloat($scope.finalPrice);
			change = change.toFixed(3).split('.');
			change[1] = change[1].slice(0,2);
			change = change.join('.');
			$scope.change = change;
		}
		var msgDisplay = {
			type: 'change',
			content: {
				cash: $scope.cash,
				change: $scope.change
			}
		};
		socket.emit('send display', msgDisplay);
	}
	$scope.updateId = $stateParams.id == null ? '' : $stateParams.id;
	if($scope.updateId != '') {
		var querySingleTransactionOut = {
			query: {
				transId: $scope.updateId
			},
			options: {}
		};
		socket.emit('fetch output transactions by query', querySingleTransactionOut);
	};
	socket.emit('fetch items by query', queryTransactionOutController);
	//Kembalian belum diatur - done
	//ng-change pas scan barcode
	//Atur output buat print css
	//CSS buat touch menu
	//bought(limit3) juga belum diberesin
});
appsocket.controller('buyerDisplayController', function(socket, $scope) {
	socket.on('receive display', function(data) {
		console.log(data);
		// if(data.type === 'lists') {
		// 	$scope.lists = data.content;
		// }
		switch(data.type) {
			case 'lists':
				$scope.lists = data.content;
				break;
			case 'total':
				$scope.sumAll = data.content.sumAll;
				$scope.finalPrice = data.content.finalPrice;
				break;
			case 'discount':
				$scope.discount = data.content;
				break;
			case 'change':
				$scope.cash = data.content.cash;
				$scope.change = data.content.change;
				break;
		}
	});
});
appsocket.controller('transactionInController', function(socket, $scope, $stateParams) {
	var lists = [];
	socket.on('fetch suppliers by query listener', function(data) {
		if(!data.error) {
			$scope.supplierLists = data.suppliers;
		}
	});
	socket.on('fetch items by query listener', function(data) {
		if(!data.error) {
			if(data.items.length > 1) {
				$scope.items = data.items;
			} else if(data.items.length === 1) {
				$scope.name = data.items[0].name;
				$scope.barcode = data.items[0].barcode[0];
				$scope.supplier = data.items[0].supplierName;
				$scope.gst = data.items[0].GST;
			} else {
				$scope.error = 'has-error';
			}
		}
	});
	socket.on('update input transaction listener', function(data) {
		if(!data.error) {
			$scope.errorMessageClass = 'bg-success';
			$scope.errorMessage = 'Stock has been updated.';
		}
	});
	socket.on('fetch input transactions by query listener', function(data) {
		if(!data.error) {
			console.log(data.transactionIn);
			$scope.lists = data.transactionIn[0].items;
			lists = $scope.lists;
		}
	});
	$scope.findItem = function() {
		if($scope.barcode != '' || $scope.barcode != null && $scope.barcode.length === 13) {
			var querySingleItem = {
				query: {
					barcode: {
						$in: [$scope.barcode]
					}
				},
				options: {}
			};
			socket.emit('fetch items by query', querySingleItem);
		}
		if($scope.name != '' || $scope.name != null) {
			var querySingleItem = {
				query: {
					name: $scope.name
				},
				options: {}
			};
			socket.emit('fetch items by query', querySingleItem);
		}
	};
	$scope.fixingValueQty = function() {
		if($scope.qty == null || $scope.qty == '') {
			$scope.qty = '';
		} else if($scope.qty < 0) {
			$scope.qty = parseFloat(0);
		} else if(isNaN($scope.qty) || isNaN(parseFloat($scope.qty))) {
			$scope.qty = parseFloat(0);
		}
	};
	$scope.fixingValueBuy = function() {
		if($scope.buy == null || $scope.buy == '') {
			$scope.buy = '';
		} else if($scope.buy < 0) {
			$scope.buy = parseFloat(0);
		} else if(isNaN($scope.buy) || isNaN(parseFloat($scope.buy))) {
			$scope.buy = parseFloat(0);
		}
	};
	$scope.addModifierLists = function() {
		var sameItem = function(el) {
			return el._id == $scope._id;
		};
		var sameSupplier = function(el) {
			return el.supplierName == $scope.supplier;
		};
		var samePrice = function(el) {
			return el.priceEach == $scope.buy;
		};
		var diffRest = function(el) {
			return el.supplierName != $scope.supplier || el.priceEach != $scope.buy ? true : false;
		}
		var added = false;
		Lazy($scope.items).each( function(el) {
			el.name == $scope.name ? $scope._id = el._id : ''
		});
		Lazy($scope.lists).filter(sameItem).filter(sameSupplier).filter(samePrice).each(function(el) { //finds the same item, supplier, and price in a row
			el.qty = $scope.qty == null || $scope.qty == '' ? parseFloat(el.qty) + parseFloat(0) : parseFloat(el.qty) + parseFloat($scope.qty);
			var totalPrice = parseFloat(el.qty) * parseFloat(el.priceEach);
			totalPrice = totalPrice.toFixed(3).split('.');
			totalPrice[1] = totalPrice[1].slice(0,2);
			totalPrice = totalPrice.join('.'); 
			el.totalPrice = totalPrice;
			$scope.errorMessage = '';
			added = true;
		});
		Lazy($scope.lists).filter(sameItem).filter(diffRest).each(function(el) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Error - Item exists in list. Please divide it to a different transaction form. This bug will be addressed in future patch.'
			added = true;
		});
		if($scope.qty == null || $scope.qty == '' || $scope.qty == 0) {
			console.log('lel');
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Quantity is empty. Please re-check.';
			added = true;
		} else if($scope.buy == null || $scope.buy == '' || $scope.buy == 0) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Buying price is empty. Please re-check.';
			added = true;
		}
		if(!added) {
			$scope.errorMessage = '';
			var totalPrice = $scope.buy * Math.abs($scope.qty);
			totalPrice = totalPrice.toFixed(3).split('.');
			totalPrice[1] = totalPrice[1].slice(0,2);
			totalPrice = totalPrice.join('.');
			lists.push({
				_id: $scope._id,
				name: $scope.name,
				barcode: $scope.barcode == null ? '' : $scope.barcode,
				qty: parseFloat($scope.qty),
				supplierName: $scope.supplier,
				priceEach: $scope.buy,
				totalPrice: totalPrice,
				gst: $scope.gst
			});
			added = false;
			$scope.lists = lists;
			$scope.name = '';
			$scope.barcode = '';
			$scope.qty = '';
			$scope.buy = '';
			$scope.gst = '';
			$scope.errorMessage = '';
		}
	};
	$scope.deleteArr = function(list) {
		var indexOf = $scope.lists.indexOf(list);
		indexOf > -1 ? $scope.lists.splice(indexOf, 1) : '';
		lists = $scope.lists;
	};
	$scope.upsertTransactionIn = function() {
		lists = angular.fromJson(angular.toJson(lists));
		if(lists == null || lists == '') {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Halt. There is no items to be written.';
		} else {	
			var queryTransactionInController = {
				query: {
					transId: $stateParams.id == null ? '' : $stateParams.id
				},
				update: {
					transId: $stateParams.id == null ? '' : $stateParams.id,
					items: lists
				}
			};
			console.log(JSON.stringify(queryTransactionInController));
			socket.emit('update input transaction', queryTransactionInController);
		}
	};
	$scope.updateId = $stateParams.id == null ? '' : $stateParams.id;
	if($scope.updateId != '') {
		var querySingleTransactionIn = {
			query: {
				transId: $scope.updateId
			},
			options: {}
		};
		socket.emit('fetch input transactions by query', querySingleTransactionIn);
	}
	socket.emit('fetch suppliers by query', query);
	socket.emit('fetch items by query', query);
});

appsocket.controller('supplierController', function(socket, $scope) {
	var supplierId;
	$scope.search = '';
	socket.on('fetch suppliers by query listener', function(data) {
		if(!data.error) {
			$scope.suppliers = data.suppliers;
		}
	});
	socket.on('update supplier listener', function(data) {
		console.log(data);
	});
	socket.emit('fetch suppliers by query', query);
	$scope.upsertSupplier = function() {
		if($scope.search.name == '' || $scope.search.name == null) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty name.';
		} else if($scope.search.phone == '' || $scope.search.phone == null) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty phone.';
		} else if($scope.search.email == '' || $scope.search.email == null) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty email.';
		} else {
			var querySupplier = {
				query: {
					_id: supplierId == null ? '' : supplierId
				},
				update: {
					name: $scope.search.name,
					address: $scope.address,
					phone: $scope.search.phone,
					email: $scope.search.email
				},
				options: {
					upsert: true
				}
			};
			$scope.search.name = '';
			$scope.address = '';
			$scope.search.phone = '';
			$scope.search.email = '';
			$scope.errorMessageClass = 'bg-success';
			$scope.errorMessage = 'Insert successfully done.';
			console.log(JSON.stringify(querySupplier));
			socket.emit('update supplier', querySupplier);
		}
	};
	$scope.getSupplierId = function() {
		Lazy($scope.suppliers).each( function(el) {
			el.name == $scope.search.name ? supplierId = el._id : '';
		});
		console.log(supplierId);
	}
});
appsocket.controller('stockController', function( socket, $scope, fileReader ) {
	$scope.gst = false;
	$scope._id = '';
	$scope.search = {};
	$scope.handling = 0;
	$scope.minStock = 0;
	socket.on('fetch items by query listener', function(data) {
		if(!data.error) {
			console.log(JSON.stringify(data.items));
			$scope.items = data.items;
		}
	});
	socket.on('fetch suppliers by query listener', function(data) {
		if(!data.error) {
			console.log(data.suppliers);
			$scope.supplierLists = data.suppliers;
		}
	});
	socket.on('update item listener', function(data) {
		if(!data.error) {
			$scope.errorMessageClass = 'bg-success';
			$scope.errorMessage = 'Insert successfully done. Do not forget to define item stock in /stock/modify.';
		}
	});
	$scope.getSellPrice = function(price, markup, handling) {
		var priceUsed;
		Lazy(price).each( function(el) {
			priceUsed = priceUsed > el.price ? priceUsed : el.price;
		});
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(markup) / 100);
		priceUsed = parseFloat(priceUsed) + (parseFloat(priceUsed) * parseFloat(handling) / 100);
		priceUsed = priceUsed.toFixed(3).split('.');
		priceUsed[1] = priceUsed[1].slice(0,2);
		priceUsed = priceUsed.join('.');
		return priceUsed;
	};
	$scope.getType = function() {
		Lazy($scope.items).each( function(el) {
			if( el.name == $scope.search.name || Lazy(el.barcode).contains($scope.search.barcode)) {
				$scope._id = el._id;
				$scope.measure = el.amountType;
				$scope.currentStock = el.currentStock;
				$scope.bought = el.bought;
				$scope.lastRestockDate = el.lastRestockDate;
				$scope.rating = el.rating;
				$scope.sold = el.sold;
				$scope.totalBought = el.totalBought;
				$scope.minStock = el.minStock;
				$scope.category = el.category;
				$scope.subCategory = el.subCategory;
				$scope.supplier = el.supplierName;
				el.barcode[0] != null ? $scope.search.barcode = el.barcode[0] : '';
				el.barcode[1] != null ? $scope.barcode = el.barcode[1] : '';
				el.barcode[2] != null ? $scope.barcodes = el.barcode[2] : '';
				$scope.gst = el.GST;
				$scope.profit = el.markUpFee;
				$scope.handling = el.handlingFee;
				$scope.isBulk = el.bulkBuying != null ? true : false;
				$scope.bulkQty = el.bulkBuying != null ? el.bulkBuying.qty : null;
				$scope.bulkPrice = el.bulkBuying != null ? el.bulkBuying.discount : null;
				$scope.picture = el.picture;
			}
		});
	};
	$scope.getBase64 = function () {
		fileReader.readAsDataURL($scope.file, $scope)
			.then( function( result ) {
				$scope.picture = result;
			});
	};
	$scope.upsertItem = function() {
		if($scope.search.name == null || $scope.search.name == '') {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty name.';
		} else if($scope.profit == null || $scope.profit == '') {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty profit margin. Is this a charity?';
		} else if(isNaN($scope.profit) || isNaN(parseFloat($scope.profit))) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Profit margin is not a number. Please re-check.';
		} else if($scope.handling < 0) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Handling fee is negative. Please re-check.';
		} else if(isNaN($scope.handling) || isNaN(parseFloat($scope.handling))) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Handling fee is not a number. Please re-check.';
		} else if($scope.supplier == null || $scope.supplier == '') {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Empty supplier.';
		} else if($scope.minStock < 0) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Minimum stock is negative. Please re-check.';
		} else if(isNaN(parseFloat($scope.minStock)) || isNaN($scope.minStock)) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Minimum stock value is not a number. Please re-check.';
		} else if($scope.isBulk && ($scope.bulkQty == null || $scope.bulkQty == '')) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk option is enabled but bulk quantity is not specified. Please specify.';
		} else if($scope.bulkQty < 0) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk quantity is negative. Please re-check.';
		} else if($scope.isBulk && (isNaN($scope.bulkQty) || isNaN(parseFloat($scope.bulkQty)))) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk quantity value is not a number. Please re-check.';
		} else if($scope.isBulk && ($scope.bulkPrice == null || $scope.bulkPrice == '')) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk option is enabled but bulk price is not specified. Please specify.';
		} else if($scope.bulkPrice < 0) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk price is negative. Please re-check.';
		} else if($scope.isBulk && (isNaN($scope.bulkPrice) || isNaN(parseFloat($scope.bulkPrice)))) {
			$scope.errorMessageClass = 'bg-danger';
			$scope.errorMessage = 'Bulk price value is not a number. Please re-check.';
		} else {
			var barcode = [];
			if($scope.search.barcode != null) {
				barcode.push($scope.search.barcode);
			}
			console.log(barcode)
			if($scope.barcode != null) {
				barcode.push($scope.barcode);
			}
			console.log(barcode)
			if($scope.barcodes != null) {
				barcode.push($scope.barcode);
			}
			if($scope._id != '') {
				var item = {
					type: 'item',
					name: $scope.search.name,
					category: $scope.category,
					subCategory: $scope.subCategory,
					supplierName: $scope.supplier,
					barcode: barcode,
					minStock: $scope.minStock,
					amountType: $scope.measure,
					bulkBuying: {
						qty: $scope.bulkQty,
						discount: $scope.bulkPrice
					},
					GST: $scope.gst == null ? false : $scope.gst,
					markUpFee: $scope.profit,
					handlingFee: $scope.handling,
					bought: $scope.bought,
					currentStock: $scope.currentStock,
					lastRestockDate: $scope.lastRestockDate,
					rating: $scope.rating,
					sold: $scope.sold,
					totalBought: $scope.totalBought,
					picture: $scope.picture
				};
			} else {
				if($scope.minStock >= 0) {
					var minStock = $scope.minStock;
					$scope.errorMessage = '';
				} else if($scope.minStock < 0) {
					var minStock = 0;
					$scope.errorMessageClass = 'bg-danger';
					$scope.errorMessage = 'Minimum stock is negative. Defaulting to 0.';
				}
				var item = {
					type: 'item',
					name: $scope.search.name,
					category: $scope.category,
					subCategory: $scope.subCategory,
					supplierName: $scope.supplier,
					barcode: barcode,
					minStock: $scope.minStock,
					amountType: $scope.measure,
					bulkBuying: {
						qty: $scope.bulkQty,
						discount: $scope.bulkPrice
					},
					GST: $scope.gst == null ? false : $scope.gst,
					markUpFee: $scope.profit,
					handlingFee: $scope.handling,
					picture: $scope.picture
				};
			}
			var queryUpsertItem = {
				query: {
					_id: $scope._id 
				},
				update: item
			};
			// console.log(JSON.stringify(queryUpsertItem));
			socket.emit('update item', queryUpsertItem);
			$scope._id = '';
		}
	};
	socket.emit('fetch suppliers by query', query);
	socket.emit('fetch items by query', query);
});
appsocket.controller('reportController', function( socket, $scope ) {
	$scope.reverse = false;
	socket.on('fetch input transactions by query listener', function(data) {
		console.log(data);
		if(!data.error) {
			var transIn = [];
			console.log(data.transactionIn);
			Lazy(data.transactionIn).each( function(el) {
				Lazy(el.items).each( function( ul ) {
					transIn.push({
						date: el.date,
						supplier: ul.supplierName,
						itemName: ul.name,
						itemQty: ul.qty,
						itemEach: ul.priceEach,
						itemTotal: ul.totalPrice,
						gst: ul.gst,
						transId: el.transId
					});
				});
			});
			console.log(transIn);
			$scope.transIn = transIn;
		}
	});

	socket.on('fetch output transactions by query listener', function(data) {
		if(!data.error) {
			var transOut = [];
			Lazy(data.transactionOut).each( function(el) {
				Lazy(el.items).each( function( ul ) {
					var discountPrice = parseFloat(ul.totalPrice) - parseFloat(ul.totalPrice) * parseFloat(el.discount) / 100;
					discountPrice = discountPrice.toFixed(3).split('.');
					discountPrice[1] = discountPrice[1].slice(0,2);
					discountPrice = discountPrice.join('.');
					transOut.push({
						date: el.date,
						transId: el.transId,
						buyer: el.buyer,
						itemName: ul.name,
						itemQty: ul.qty,
						itemEach: ul.priceEach,
						discount: el.discount,
						itemTotal: ul.totalPrice,
						discountPrice: discountPrice
					});
				});
			});
		}
		$scope.transOut = transOut;
	});

	$scope.beginOpen = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.beginOpened = true;
	};
	$scope.endOpen = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.endOpened = true;
	};
	$scope.queryReport = function() {
		var queryReportController = {
			query: {
				date: {
					$gte: $scope.beginDate == null ? $scope.beginDate = '' : $scope.beginDate.setDate($scope.beginDate.getDate()),
					$lte: $scope.endDate == null ? $scope.endDate = new Date() : $scope.endDate.setDate($scope.endDate.getDate() + 1)
				}
			},
			options: {}
		};
		socket.emit('fetch input transactions by query', queryReportController);
		socket.emit('fetch output transactions by query', queryReportController);
	};
	$scope.gstValue = function(total, gst) {
		if(gst) {
			var gstVal = parseFloat(total) * parseFloat(10) / parseFloat(100);
			gstVal = gstVal.toFixed(3).split('.');
			gstVal[1] = gstVal[1].slice(0,2);
			gstVal = gstVal.join('.');
			return 'Yes, $' + gstVal;
		} else {
			return 'No'
		}
	};
});

appsocket.controller('reorderController', function(socket, $scope) {
	var itemList = '';
	socket.on('get depleted items listener', function(data) {
		if(!data.error) {
			if(data.depletedItems.length > 0) {
				$scope.depletedItems = data.depletedItems;
			} else {
				$scope.depletedItems = [{
					name: 'Empty',
					stock: 'You have no depleted item'
				}];
				console.log($scope.depletedItems);
			}
		}
	});
	socket.on('fetch items by query listener', function(data) {
		if(!data.error) {
			$scope.items = data.items;
		}
	});
	socket.on('fetch suppliers by query listener', function(data) {
		if(!data.error) {
			$scope.suppliers = data.suppliers;
		}
	});
	socket.on('send email listener', function(data) {
		if(!data.error) {
			console.log(data);
		}
	});
	$scope.sendEmailRecipient = function() {
		Lazy($scope.suppliers).each( function(el) {
			if(el.name === $scope.supplier) {
				$scope.emailRecipient = el.name;
				$scope.emailRecipientAddress = el.email;
			}
		});
	};
	$scope.sendEmailSubject = function() {
		$scope.emailSubject = $scope.subject;
	}
	$scope.sendEmailHeader = function() {
		$scope.emailHeader = $scope.header.replace(/\n/g, '<br />');
	};
	$scope.sendItemList = function() {
		itemList += $scope.name + ' - ' + $scope.qty + ' ' + $scope.measure + '\n';
		$scope.emailContent = '<br />'.concat(itemList.replace(/\n/g, '<br />')); 
	};
	$scope.sendEmailFooter = function() {
		$scope.emailFooter = '<br />'.concat($scope.footer.replace(/\n/g, '<br />'));
	};
	$scope.sendEmail = function() {
		var msg = {
			supplierName: $scope.emailRecipient,
			supplierEmail: $scope.emailRecipientAddress,
			subject: $scope.subject,
			text: $scope.header + '\n\n' + itemList + '\n\n' + $scope.footer
		};
		console.log(msg);
		socket.emit('send email', msg);
	};
	socket.emit('fetch suppliers by query', query);
	socket.emit('fetch items by query', query);
	socket.emit('get depleted items', query);
	// Get items list
	// Get supplier list
	// Warning if current supplier name != yang ada di item list
	// Parse lists into email-friendly form; do not directly write lists into $scope for email preview
});
appsocket.config( function( $stateProvider, $urlRouterProvider ) {
	$urlRouterProvider.otherwise('/transaction');

	$stateProvider
		.state('transaction', {
			url: '/transaction',
			templateUrl: 'assets/templates/transaction.html',
			controller: 'transactionOutController'
		})
		.state('editTransactionOut', {
			url: '/transaction/edit/:id',
			templateUrl: 'assets/templates/transaction.html',
			controller: 'transactionOutController'
		})
		.state('stock', {
			url: '/stock',
			templateUrl: 'assets/templates/stock.html',
			controller: 'stockController'
		})
		.state('supplier', {
			url: '/supplier',
			templateUrl: 'assets/templates/supplier.html',
			controller: 'supplierController'
		})
		.state('report', {
			url: '/report',
			templateUrl: 'assets/templates/report.html',
			controller: 'reportController'
		})
		.state('editItemStock', {
			url: '/stock/modify',
			templateUrl: 'assets/templates/transin.html',
			controller: 'transactionInController'
		})
		.state('editItemStockExisting', {
			url: '/stock/modify/:id',
			templateUrl: 'assets/templates/transin.html',
			controller: 'transactionInController'
		})
		.state('buyerDisplay', {
			url: '/buyer/display',
			templateUrl: 'assets/templates/display.html',
			controller: 'buyerDisplayController'
		})
		.state('depletedItems', {
			url: '/stock/reorder',
			templateUrl: 'assets/templates/reorder.html',
			controller: 'reorderController'
		});
});

// TODO
// Create a directive for checking all number input or maybe utilize ng-pattern
// Modularize things maybe.