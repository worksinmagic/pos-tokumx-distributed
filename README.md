# Piece of Suit
========
A Node.js backed point of sales system using TokuMX as a distributed database through replica sets.
**Please do not release this document for public consumption**

###### The architecture
```
========          ========          ========
  App0              App1              AppN
----------------------------------------------- ...
  DB0  - - - - - -  DB1 - - - - - - - DBN - - - ...
========          ========          ========
```

### Dependencies

###### Hardware and Softwares
- GNU/Linux Ubuntu x86-64
- Minimum RAM 512MB, recommended more than 4GB for very large dataset (Warning: consider using more than 4GB for more than 4 machines for guaranteed performance)
- A switch or router
- Computer(s) with ethernet port or wifi adapter
- Printer(s)
- Barcode scanner(s) (Optional)

###### Back-End

* Node.js 0.10.x or 0.11.x, if 0.10.x, require gnode
* TokuMX or MongoDB
* Socket.io 1.x
* Logbook
* Mongoskin
* Node-uuid
* Lazy.js
* Suspend
* Bcrypt

###### Front-End

Do this for initial set-up:
```
npm install
bower install
```

After that, just run
`gulp`
in the pos-tokumx-distributed root folder to watch any changes done in js and scss folders. Also: using gulp-sass v0.7.1

### Setup and Installation
Note: You **must** have internet connection on each machine to follow these steps.

Set your machines to use static IP, probably in 192.168.x.x space and stop DHCP on your router.

Install Node.js first, follow these steps:
- `sudo add-apt-repository ppa:chris-lea/node.js`
- `sudo apt-get update`
- `sudo apt-get install nodejs`

Clone this repo and install the dependencies:
- `git clone thisrepo /home/yourusername/pos`
- `cd /home/yourusername/pos`
- `sudo apt-get install build-essential`
- `sudo npm install`

Install TokuMX, follow these steps:
- `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key 505A7412`
- `echo "deb [arch=amd64] http://s3.amazonaws.com/tokumx-debs $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/tokumx.list`
- `sudo apt-get update`
- `sudo apt-get install tokumx`

Now we set the replica sets for TokuMX.
- Open `/etc/hosts`
- Adjust it depending on your network settings

```
# For simplicity, we use posN as our hosts name
127.0.0.1 localhost pos0

# IPs of your machines in the network
192.168.0.1 pos0
192.168.0.2 pos1
# etc
```

Next,
- `sudo hostname pos0` on the first machine, change it to `pos1` on second machine, etc
- Open `/etc/hostname` and edit it to `pos0` or `pos1` on second machine, etc
- Stop TokuMX server on each machine by running `sudo service tokumx stop`
- Backup `/etc/tokumx.conf` first, and then edit it

```
# Note, only edit these points
port = 27017
cacheSize = 3G # Use approximately 75% of RAM
directio = true
diaglog = 1
nohttpinterface = true
pidfilepath = /var/run/tokumx/tokumx.pid
replSet = rs0
```

At last,
- `sudo service tokumx start`
- On one of your machine, run next steps to set it as the replica set's master
- `mongo`
- On the prompt, enter `rs.initiate()`
- `rs.add("pos1")`
- And then restart the server `sudo service tokumx restart`

Your replica sets is now ready.

### Setup NTP

Open up `/etc/ntp.conf`

And then, commented out the previous server's definition with your machines' hostnames.

```
# server 0.ubuntu.pool.ntp.org
# server 1.ubuntu.pool.ntp.org
# server 2.ubuntu.pool.ntp.org
# server 3.ubuntu.pool.ntp.org

# server ntp.ubuntu.com

server pos0
server pos1

# add these
restrict 192.168.0.0 mask 255.255.255.0 nomodify notrap
broadcast 192.168.0.255
disable auth
broadcastclient
```

*Front-end Developers add your steps here*
What steps m8. Just click here and there.


### Enterprise version

* 24/7 online technical support
* Optional training package
* Provide cloud backups
* Provide real-time system
* Provide database monitoring software

==================================================

Future updates after refactoring
- A "stockOpname is done" or something like that. Forbidding editing any transaction after that save point.
